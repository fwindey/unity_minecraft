# README #

On frederikwindey.blog you can find a tutorial how to make your first game in Unity. The tutorial is using Minecraft as an inspiration.
This project is the final result of that tutorial. 

### How to run it ###

Download this repository.
Download the latest version of Unity via www.unity3d.com and install it.
Open the project in Unity and press play.

### Any questions? ###

You can contact the creator through frederikwindey.blog