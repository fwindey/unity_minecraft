﻿using UnityEngine;
using System.Collections;

public class Walk : MonoBehaviour {

    public float WalkSpeed;
    public float RotationSpeed;
    public float JumpSpeed;
    public float Gravity;

    private CharacterController controller;
    private Animation animations;
    private Vector3 moveDirection;
    private float UpSpeed;

    void Start ()
    {
        animations = GetComponent<Animation>();
        controller = GetComponent<CharacterController>();
    }
	
	void Update ()
    {
        bool isMoving = false;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            isMoving = true;
            moveDirection = transform.forward * WalkSpeed * Time.deltaTime;
        }
        else if(Input.GetKey(KeyCode.DownArrow))
        {
            isMoving = true;
            moveDirection = -transform.forward * WalkSpeed * Time.deltaTime;
        }
        else
        {
            moveDirection = Vector3.zero;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, -RotationSpeed * Time.deltaTime, 0);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, RotationSpeed * Time.deltaTime, 0);
        }

        if(!isMoving)
        {
            animations.CrossFade("Idle");
        }
        else
        {
            animations.CrossFade("Walk");
        }

        if (controller.isGrounded)
        {
            UpSpeed = -Gravity;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                UpSpeed = JumpSpeed;
            }
        }
        else
        {
            UpSpeed -= Gravity;
        }
        moveDirection.y = UpSpeed * Time.deltaTime;

        controller.Move(moveDirection);
    }
}
