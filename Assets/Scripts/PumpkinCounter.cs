﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PumpkinCounter : MonoBehaviour
{
    public int Pumpkins = 0;
    public float Timer = 0;

    public Text PumpkinText;
    public Text TimerText;
    public Text GameOverText;

    private void Start()
    {
        PumpkinText.text = "Pumpkins: 0";
        GameOverText.enabled = false;
    }

    private void Update()
    {
        Timer += Time.deltaTime;
        TimerText.text = Timer.ToString("0.00s");
    }

    private void OnTriggerEnter(Collider other)
    {
        Pumpkins++;
        PumpkinText.text = "Pumpkins: " + Pumpkins;
        if (Pumpkins == 5)
        {
            enabled = false;
            GameOverText.enabled = true;
        }
    }
}
