﻿using UnityEngine;
using System.Collections;

public class HarvestPumpkins : MonoBehaviour {

    public Transform LeftHand;
    public LayerMask PumpkinsLayer;
    public float ThrowSpeed;

    private Animation animations;
    private bool hasPumpkin = false;
    private GameObject pickedUpPumpkin;

    void Start()
    {
        animations = GetComponent<Animation>();
    }

    void Update ()
    {
	    if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            animations.CrossFade("Use", 0, PlayMode.StopAll);

            if (!hasPumpkin)
            {
                Collider[] pumpkins = Physics.OverlapSphere(LeftHand.position, 0.75f, PumpkinsLayer);
                if (pumpkins.Length > 0)
                {
                    Vector3 pumpkinScale = pumpkins[0].transform.localScale;
                    pumpkinScale = pumpkinScale * 0.5f;
                    pumpkins[0].transform.localScale = pumpkinScale;
                    pumpkins[0].transform.parent = LeftHand;
                    pumpkins[0].transform.localPosition = new Vector3(0, 0, 0);
                    pumpkins[0].gameObject.layer = 0;

                    hasPumpkin = true;
                    pickedUpPumpkin = pumpkins[0].gameObject;
                }
            }
            else
            {
                pickedUpPumpkin.transform.parent = null;
                Rigidbody rb = pickedUpPumpkin.AddComponent<Rigidbody>();
                rb.velocity = transform.forward * ThrowSpeed/Mathf.Sqrt(2) + transform.up * ThrowSpeed / Mathf.Sqrt(2);
                hasPumpkin = false;
            }
        }
	}
}
